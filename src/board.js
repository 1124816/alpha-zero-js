let board = {
    getLegalMoves: function(color) {
        let moves = [];

        for(square in this.nextmove) {
            if(this.metapieces[square] === 0 && this.nextmove[square] === 1 ) {
                for(let x=0; x<9; x++) {
                    if(this.pieces[square][x] === 0) {
                        let newmove = [square, x]
                        moves.push(newmove);
                    };
                };
            };
        };

        return moves;
    }.
    hasLegalMove: function() {
        for(square in this.nextmove) {
            if(this.metapieces[square] === 0 && this.nextmove[square] === 1 ) {
                for(let x=0; x<9; x++) {
                    if(this.pieces[square][x] === 0) {
                        return true
                    };
                };
            };
        };
        return false
    },
    hasWonSquare: function(square) {
        let winningpos = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];

        for(i of winningpos) {
            if(square[i[0]] === square[i[1]] && square[i[1]] === square[i[2]] && square[i[0]] != 0) {
                return square[i[0]]
            };
        };
        return false
    },
    isWon: function(color) {
        return color == this.hasWonSquare(this.metapieces)
    },
    executeMove: function(move, color) {
        this.nextmove = [0,0,0,0,0,0,0,0,0];
        this.pieces[move[0]][move[1]] = color;

        for(let i=0; i<this.n; i++) {
            this.metapieces[i] = Math.abs(this.hasWonSquare(this.pieces[i]));
        };

        if(this.metapieces[move[1]] === 0) {
            this.nextmove[move[1]] = 1;
        } else {
            this.nextmove = this.metapieces.map(x => 1-x);
        };
    }
};

export default function(pieces = false, nextmove = false, metapieces = false, n=9) {
    let board = Object.assign({}, board);
    board.n = n;


    if(pieces) {
        board.pieces = pieces.slice();
    } else {
        board.pieces = new Array(board.n+1);
        for(let i=0; i<board.n; i++) {
            board.pieces[i] = [0,0,0,0,0,0,0,0,0];
        };
    };


    if(nextmove) {
        board.nextmove = nextmove.slice();
    } else {
        board.nextmove = [0,0,0,0,1,0,0,0,0];
    };


    if(metapieces) {
        board.metapieces = metapieces.slice();
    } else {
        board.metapieces= [0,0,0,0,0,0,0,0,0,0];
    };
};  
