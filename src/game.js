import Board from '/board.js';

let game = {
    getBoardSize: function() {
        return [this.n+1, this.n];
    },
    getActionSize: function() {
        return this.n*this.n+1;
    },
    getNextState: function(board, player, action) {
        if(action === this.n*this.n) {
            return [board, -player];
        };
        b = Board(this.n);
        b.pieces = board.pieces.slice();
    },
};

export default function(n=9) {
    return Object.assign({}, game);
};
